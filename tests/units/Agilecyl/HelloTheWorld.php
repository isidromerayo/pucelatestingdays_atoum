<?php
namespace tests\units\Agilecyl;

/**
 * Description of HelloTheWorld
 *
 * @author isidromerayo
 */
class HelloTheWorld extends \atoum\test {
    public function testGetHiAtoum() {
        $helloToTest = new \Agilecyl\HelloTheWorld();
        $this->assert
                    //we expect the getHiAtoum method to return a string
                    ->string($helloToTest->getHiAtoum())
                    //and the string should be Hi atoum !
                    ->isEqualTo('Hi atoum !');
    }
}