<?php
namespace tests\units\Agilecyl;

/**
 * Description of Demo
 *
 * @author isidromerayo
 */
class Area extends \atoum\test {

    
    function testTriangle()
    {
        $area = new \Agilecyl\Area();
        $result = $area->triangle(6, 10);
        $this->integer($result)->isEqualTo(30);
    }

    function testRectangle()
    {
        $area = new \Agilecyl\Area();
        $result = $area->rectangle(2, 5);
        $this->integer($result)->isEqualTo(10);
    }

    function testSquare()
    {
        $area = new \Agilecyl\Area();
        $result = $area->square(4);
        $this->boolean(is_numeric($result))->isTrue();
    }
}

